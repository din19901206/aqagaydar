package ru.homework5.oop;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        Double result = null;
        //создаем массив
        String[] numbers;
        System.out.println("Введите математическую операцию");
        Scanner scanner = new Scanner(System.in);
        String operation = scanner.nextLine();
        //удаляем пробелы
        operation = operation.replaceAll(" ", "");

        //проверка по регулярному выражению
        String pattern = "^\\d+?[-+*/]\\d+?$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(operation);

        //если по регулярному прошли успешно, то переходим к операциям
        if (m.matches()){
            if (operation.indexOf('+') > 0) {
                numbers = operation.split("[+]");
                result = Double.parseDouble(numbers[0]) + Double.parseDouble(numbers[1]);
            }
            else if (operation.indexOf('-') > 0) {
                numbers = operation.split("[-]");
                result = Double.parseDouble(numbers[0]) - Double.parseDouble(numbers[1]);
            }
            else if (operation.indexOf('*') > 0) {
                numbers = operation.split("[*]");
                result = Double.parseDouble(numbers[0]) * Double.parseDouble(numbers[1]);
            }
            else if (operation.indexOf('/') > 0) {
                numbers = operation.split("[/]");
                result = Double.parseDouble(numbers[0]) / Double.parseDouble(numbers[1]);
            }
            System.out.println("Ответ:" + "\n" + result);

        }
        else { System.out.println("Введен некорректный символ"); }

    }
}