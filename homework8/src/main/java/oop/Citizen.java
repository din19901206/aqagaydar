package oop;

public abstract class Citizen {
    protected String name;
    protected String lastname;
    protected Race race;

    @Override
    public String toString(){
        return name + " " + lastname + " (" + race + ")" ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Race getRace() {
        return race;
    }

    //используем, чтобы перезаписать Расу
    public void setRace(Race race) {
        this.race = race;
    }
}
