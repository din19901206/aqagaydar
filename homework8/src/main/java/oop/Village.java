package oop;

import com.github.javafaker.Faker;

import java.util.*;

public class Village {
    private Random random = new Random();
    private int citizenQuantity = 50;
    private Calendar startLife = new GregorianCalendar(1650, 0, 1);
    private Calendar endLife = new GregorianCalendar(1750, 0, 1);
    private int meetWitchWerewolf = 0;

   /* public List<Citizen> getCitizens() {
        return citizens;
    } */

    private List<Citizen> citizens = new ArrayList<>();
    private int daysHavePassed = 0;

    private Citizen getRace(int race){
        Citizen citizen = null;
        Faker faker = new Faker();
        switch (race){
            case 0:
                citizen = new Vampire(faker.name().firstName(), faker.name().lastName());
                break;
            case 1:
                citizen = new Witch(faker.name().firstName(), faker.name().lastName());
                break;
            case 2:
                citizen = new Werewolf(faker.name().firstName(), faker.name().lastName());
                break;
            case 3:
                citizen = new Peasant(faker.name().firstName(), faker.name().lastName());
                break;
            case 4:
                citizen = new Headhunter(faker.name().firstName(), faker.name().lastName());
                break;
        }
        return citizen;
    }

    public void generateCitizens(){
        for (int i = 0; i< citizenQuantity;i++){
            citizens.add(getRace(random.nextInt(5)));
        }
    }

    public void visit(Citizen citizenOne, Citizen citizenTwo, Calendar calendar){
        //Любой житель (кроме Witch) встречается с Witch в пятницу 13-е или 31 октября
        if (!citizenOne.getRace().equals(Race.WITCH) && citizenTwo.getRace().equals(Race.WITCH) && ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && calendar.get(Calendar.DAY_OF_MONTH)==13)
                || (calendar.get(Calendar.MONTH) == Calendar.OCTOBER && calendar.get(Calendar.DAY_OF_MONTH) == 31))){
            try {
                new MurderException("MurderException");
            } catch (Exception e){
                e.printStackTrace();
            }
            System.out.println("MurderException " + citizenOne + " был убит " + citizenTwo);
            //удаляем жителя из списка
            citizens.remove(citizenOne);
        }
        //Определяем, что второй житель вампир
        else if (citizenTwo.getRace().equals(Race.VAMPIRE)){
            //Определяем удачу
            int Luck = random.nextInt(100);
            //Если житель Peasant или Witch 5% шанс, что житель становится Vampire
            if ( 5<= Luck && (citizenOne.getRace().equals(Race.WITCH)|| citizenOne.getRace().equals(Race.PEASANT))){
                citizenOne.setRace(Race.VAMPIRE);
                System.out.println("Житель становится " + citizenOne);
            }
            //1% Вызывается DawnException и Vampire исчезает из деревни.
            else if (Luck <=1){
                try {
                     new DawnException("DawnException");
                } catch (Exception e){
                    e.printStackTrace();
                }
                System.out.println("DawnException " + citizenTwo + " исчезает из деревни");
                //удаляем жителя из списка
                citizens.remove(citizenTwo);
            }
            //5% Вызывается MurderException и житель исчезает из деревни
            else if (Luck <=5){
                try {
                     new MurderException("MurderException");
                } catch (Exception e){
                    e.printStackTrace();
                }
                System.out.println("MurderException " + citizenOne + " исчезает из деревни");
                //удаляем жителя из списка
                citizens.remove(citizenOne);
            }
            else {System.out.println(citizenOne + " встречается с " + citizenTwo);}
        }
        //первое полнолуние было 28.01.1650 года. После этой даты полнолуние происходит каждые 28 суток.
        else if (citizenTwo.getRace().equals(Race.WEREWOLF) && (daysHavePassed - 28)%28 ==0){
            try {
                new MurderException("MurderException");
            } catch (Exception e){
                e.printStackTrace();
            }
            //условие слишком жесткое, для проверки убирал ведьму и уменьшал количество дней полнолуния
            if (citizenOne.getRace().equals(Race.WITCH) && calendar.get(Calendar.MONTH) == Calendar.OCTOBER && calendar.get(Calendar.DAY_OF_MONTH) == 31){
                //учет встреч Ведьмы с Оборотнем
                meetWitchWerewolf++;
                System.out.println("Количество встреч ведьмы с оборотнем в полнолуние 31 октября " + meetWitchWerewolf);
            }
            System.out.println("MurderException" + citizenOne + " исчезает из деревни");
            //удаляем жителя из списка
            citizens.remove(citizenOne);
        }
        //если крестьянин идет в гости к охотнику за головами, то тот делится с ним опытом
        else if (citizenOne.getRace().equals(Race.PEASANT) && citizenTwo.getRace().equals(Race.HEADHUNTER)){
            System.out.println(citizenTwo + " делится опытом с " + citizenOne);

        }
        else {System.out.println("Происходит просто чаепитие");}
    }

    public void LifeCycle(){
        generateCitizens();
        while (startLife.before(endLife)){
            //добавляем счетчик на количество дней
            daysHavePassed++;
            int action = random.nextInt(4);
            //System.out.println("Сейчас жителей " + citizens.size());
            //System.out.println("Будет действие " + action);
            System.out.println(startLife.get(Calendar.DAY_OF_MONTH) + " " + startLife.get(Calendar.MONTH) + " " + startLife.get(Calendar.YEAR));
            switch (action){
                case 0:
                    Citizen citizenAdd = getRace(random.nextInt(5));
                    citizens.add(citizenAdd);
                    System.out.println(citizenAdd + " приехал в деревню" );
                    break;
                case 1:
                    if (!citizens.isEmpty()){
                        int randomCitizen = random.nextInt(citizens.size());
                        Citizen citizenRemove = citizens.remove(randomCitizen);
                        System.out.println(citizenRemove + " уехал из деревни");
                    } else {
                        System.out.println("Жителей не осталось");
                    }
                    break;
                case 2:
                    if (!citizens.isEmpty()){
                        int randomCitizenOne = random.nextInt(citizens.size());
                        int randomCitizenTwo = random.nextInt(citizens.size());
                        visit(citizens.get(randomCitizenOne), citizens.get(randomCitizenTwo), startLife);
                        break;
                    }
                case 3:
                    System.out.println("Все спят, ничего не происходит");
                    break;
            }
            startLife.add(Calendar.DATE, 1);
        }
    }
}


























