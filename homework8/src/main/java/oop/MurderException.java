package oop;

public class MurderException extends Exception{

    public MurderException(String message){
        super(message);
    }

}
