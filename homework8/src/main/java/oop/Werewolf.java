package oop;

public class Werewolf extends Citizen{
    public Werewolf(String name, String lastname) {
        race = Race.WEREWOLF;
        this.name = name;
        this.lastname = lastname;
    }
}
