package oop;

public class Witch extends Citizen{

    public Witch(String name, String lastname) {
        race = Race.WITCH;
        this.name = name;
        this.lastname = lastname;
    }

}