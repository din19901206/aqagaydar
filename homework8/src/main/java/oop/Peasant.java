package oop;

public class Peasant extends Citizen{
    public Peasant(String name, String lastname){
        race = Race.PEASANT;
        this.name = name;
        this.lastname = lastname;
    }
}
