import Application.credit.ClientData;
import Application.credit.MyJson;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static io.restassured.RestAssured.given;

public class CreditTest {

    @DataProvider
    private Object[][] users(){
        return new Object[][]{
                {"Vova", 40, 500},
                {"Sahsa", 50, 20},
                {"Elena", 15, 300},
                {"Oleg", 20, 1800},
        };
    }

    @Test(dataProvider = "users")
    public void createClient(String name, Integer age, Integer sum){
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("name", name);
        bodyJson.put("age", age);
        bodyJson.put("sum", sum);

        given().contentType(ContentType.JSON).body(bodyJson.toString()).post("http://localhost:8080/clients")
                .then().log().all().assertThat().statusCode(200);
    }

    @Test
    public void deleteClient(){
        given().get("http://localhost:8080/clients/delete/1650362486650").then().log().all().assertThat().statusCode(200);
    }

    @Test
    public void check() throws IOException{
        System.out.println(getResult());
    }

    public String getResult() throws IOException {
        Path path = Paths.get("D:\\git\\aqagaydar\\homework19\\src\\main\\resources\\resources1650511797803");
        int age = 0;
        int sum = 0;
        String name = "";
        String[] params = Files.readAllLines(Paths.get(String.valueOf(path))).toString().split(",");
        for(int i = 0; i < params.length; i++){
            if(params[i].contains("age")){
                String[] values1 = params[i].split(":");
                age = Integer.parseInt(values1[1]);
            }
            if(params[i].contains("sum")){
                String[] values2 = params[i].split(":");
                sum = Integer.parseInt(values2[1]);
            }
            if(params[i].contains("name")){
                String[] values3 = params[i].split(":");
                name = values3[1].replace("\"", "");
            }
        }
        return  resultCredit(name,age,sum);
    }

    private String resultCredit(String name, int age, int sum) {
        return (age >= 18) && (!name.equals("Bob")) && (sum < age * 100) ? "Кредит одобрен"
                : "Отказано в кредите";
    }

    @Test
    public void checkAllClients(){
        final List<ClientData> clientData = given().contentType(ContentType.JSON)
                .get("http://localhost:8080/clients/allInfo")
                .then().log().all()
                .extract().body().jsonPath().getList("clients", ClientData.class);
        int sizeActual = clientData.size();
        int sizeExpected = MyJson.showAll().size();
        Assert.assertEquals(sizeActual,sizeExpected);
    }

    @Test
    public void checkIdExist(){
        final List<ClientData> clientData = given().contentType(ContentType.JSON)
                .get("http://localhost:8080/clients/allInfo")
                .then().log().all()
                .extract().body().jsonPath().getList("clients", ClientData.class);
        clientData.forEach(x->Assert.assertNotNull(x.id));

        for(int i = 0; i<clientData.size(); i++){
            Assert.assertNotNull(clientData.get(i).id);
        }
    }

    @Test
    public void negativeGetUser(){
        String error = given().contentType(ContentType.JSON).get("http://localhost:8080/clientsInfo/1650362486618")
                .then().log().all().extract().jsonPath().getString("error");
        Assert.assertEquals(error, "Пользователя с таким айди не существует");
    }

    //Для дебага
    /*@Test
    public void getAll() throws IOException {
        MyJson.showAll();
    }*/

    @Test
    public void clientByIdMatchesShema(){
        given().get("http://localhost:8080/clientsInfo/1650511797803").then().log().all()
                .assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("swagger.json"));
    }

}
