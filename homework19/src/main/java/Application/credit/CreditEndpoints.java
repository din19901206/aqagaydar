package Application.credit;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
public class CreditEndpoints {
    private String basePath = "D:\\git\\aqagaydar\\homework19\\src\\main\\resources\\resources";
    private ObjectMapper mapper = new ObjectMapper();

    //Работает с тестом
    @Operation(description = "Создание клиента")
    @PostMapping(path = "/clients", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ClientData createClient(@RequestBody ClientDataTemp temp) throws IOException {
        ClientData clientData = temp.toClient();
        clientData.saveClient(basePath + clientData.id, mapper);
        return clientData;
    }

    //удаление клиента по id
    @Operation(description = "Удаление клиента по id")
    @GetMapping(path = "/clients/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object deleteClient(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get(basePath + id);
        return Files.deleteIfExists(path);
    }

    //информация о клиенте по id
    @Operation(description = "Получение информации о клиенте")
    @GetMapping(path = "/clientsInfo/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getRequestJson(@PathVariable(required = false) String id) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error", "Пользователя с таким айди не существует");
        try{
            Path path = Paths.get(basePath + id);
            return Files.readAllLines(Paths.get(String.valueOf(path))).toString();
        } catch (Exception e){
            return jsonObject.toString();
        }
    }

    //информация о статусе выдачи кредита
    @Operation(description = "Получение информации о статусе выдачи кредита")
    @GetMapping(path = "/getResult/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getResult(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get(basePath + id);
        int age = 0;
        int sum = 0;
        String name = "";
        String[] params = Files.readAllLines(Paths.get(String.valueOf(path))).toString().split(",");
        for(int i = 0; i < params.length; i++){
            if(params[i].contains("age")){
                String[] values1 = params[i].split(":");
                age = Integer.parseInt(values1[1]);
            }
            if(params[i].contains("sum")){
                String[] values2 = params[i].split(":");
                sum = Integer.parseInt(values2[1]);
            }
            if(params[i].contains("name")){
                String[] values3 = params[i].split(":");
                name = values3[1].replace("\"", "");
            }
        }
        return  resultCredit(name,age,sum);
    }

    private String resultCredit(String name, int age, int sum) {
        return (age >= 18) && (!name.equals("Bob")) && (sum < age * 100) ? "Кредит одобрен"
                : "Отказано в кредите";
    }

    @Operation(description = "Вывод всех клиентов")
    @GetMapping (path = "/clients/all",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getAll(){
        List<String> lst= MyJson.showAll();
        return lst;
    }

    @GetMapping(path = "/clients/allInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getAllInfo() throws IOException {
        return MyJson.showRealInfo();
    }
}

























