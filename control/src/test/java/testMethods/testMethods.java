package testMethods;

import oop.TestHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

public class testMethods {

    @Test
    public void getRandomlongTest(){
        Long randomlongTest = TestHelper.getRandomlong();
        Assertions.assertTrue(String.valueOf(randomlongTest).length() >18 &&
                String.valueOf(randomlongTest).length()<21);
        Assertions.assertNotNull(randomlongTest);
    }

    @Test
    public void getRandomNumberTest(){
        int randomNumberTest = TestHelper.getRandomNumber();
        Assertions.assertNotNull(randomNumberTest);
        Assertions.assertTrue(randomNumberTest>1969 && randomNumberTest<2023);
    }

    @Test
    public void getLineTest(){
        String lineTest = TestHelper.getLine();
        Assertions.assertEquals(20,lineTest.length());
        Assertions.assertNotNull(lineTest);
    }

    @Test
    public void getDateTimeFormatterTest(){
        String DateTimeFormatterTest = TestHelper.getDateTimeFormatter();
        Assertions.assertEquals("2017/07/26", DateTimeFormatterTest);
        Assertions.assertNotNull(DateTimeFormatterTest);
    }

    @Test
    public void getDoubleStringTest(){
        Double DoubleStringTest = TestHelper.getDoubleString();
        Assertions.assertNotNull(DoubleStringTest);
        Assertions.assertTrue(DoubleStringTest==5.646E12 || DoubleStringTest==Double.POSITIVE_INFINITY);
    }

    @Test
    public void getHashMapTest() throws IOException {
        Map<String, String> HashMapTest = TestHelper.getHashMap();
        Assertions.assertNotNull(HashMapTest);
        Assertions.assertEquals(5, HashMapTest.size());
    }
}
