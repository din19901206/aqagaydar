package oop;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

public abstract class TestHelper {

    public static long getRandomlong(){
        long randomNumberLong = generateLong();
        System.out.println(randomNumberLong);
        return randomNumberLong;
    }

    private static long generateLong(){
        System.out.println("Выдает случайное число типа long:");
        return new Random().nextLong();
    }

    public static int getRandomNumber(){
        int randomNumber = generateNumber();
        System.out.println(randomNumber);
        return randomNumber;
    }

    private static int generateNumber(){
        System.out.println("Выдает случайное число от 1970 до текущего года:");
        return new Random().nextInt(1970,2023);
    }

    public static String getLine(){
        String line = generateLine();
        return line;
    }

    private static String generateLine (){
        int lenght = 20;
        String higherCase = "QWERTYUIOPASDFGHJKLZXCVBNM";
        String lowerCase = "qwertyuiopasdfghjklzxcvbnm";
        String space = " ";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<lenght;i++){
            if (i ==0 || i==7 || i ==14){
                int number=random.nextInt(26);
                sb.append(higherCase.charAt(number));
            }
            else if (i ==6 || i ==13 ){
                sb.append(space);
            }
            else if ((i >=1 && i<=5) || (i >=8 && i<=12) || i >=15){
                int number=random.nextInt(26);
                sb.append(lowerCase.charAt(number));
            }
        }
        System.out.println("Генерирует случайную строку:");
        System.out.println(sb);
        return sb.toString();
    }

    public static String getDateTimeFormatter(){
        String newDateTime = generateDateTimeFormatter();
        return newDateTime;
    }

    private static String generateDateTimeFormatter(){
        final DateTimeFormatter OLD_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        final DateTimeFormatter NEW_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        String oldDateTime = "26/07/2017";
        LocalDate date = LocalDate.parse(oldDateTime, OLD_FORMATTER);
        String newDateTime = date.format(NEW_FORMATTER);
        System.out.println("Преобразует один формат даты в другой:");
        System.out.println(newDateTime);
        return newDateTime;
    }

    public static Double getDoubleString(){
        Double doubleString = generateDouble();
        return doubleString;
    }

    private static Double generateDouble(){
        String str = "564.6e10";
        try {
            Double doubleString = Double.valueOf(str);
            System.out.println("Превращает строку в число Double:");
            System.out.println(doubleString);
            return doubleString;
        } catch (Exception e) {
            double inf = Double.POSITIVE_INFINITY;
            System.out.println(inf + 5);
            return inf;
        }
    }

    public static Map<String, String> getHashMap() throws IOException {
        Map<String,String> map = readingHashMap();
        return map;
    }

    private static Map<String, String> readingHashMap() throws IOException {
        Map<String,String> map = new LinkedHashMap<>();
        String delimiter = ":";
        Stream<String> lines = Files.lines(Paths.get("readfile.txt"));
        lines.filter(line ->line.contains(delimiter)).forEach(line->map.put(line.split(delimiter)[0], line.split(delimiter)[1]));
        System.out.println("Прочитает файл из нескольких строк и составит из них HashMap:");
        System.out.println(map);
        return map;
    }
}
