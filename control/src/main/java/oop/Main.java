package oop;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        //выдает случайное число типа long
        TestHelper.getRandomlong();
        //выдает случайное число от 1970 до текущего года
        TestHelper.getRandomNumber();
        //генерирует случайную строку (три слова c больших букв разумной длины через пробел)
        TestHelper.getLine();
        //преобразует один формат даты в другой
        TestHelper.getDateTimeFormatter();
        //превращает строку в число Double и, если это невозможно, возвращает Infinity
        TestHelper.getDoubleString();
        //прочитает файл из нескольких строк (каждая в формате « Ключ :: Значение » ) и составит из них HashMap.
        TestHelper.getHashMap();
    }
}







