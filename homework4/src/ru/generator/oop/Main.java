package ru.generator.oop;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Определяем уровень сложности
        System.out.println("Введи цифру для выбора уровня сложности");
        System.out.println("1 - Легко");
        System.out.println("2 - Средне");
        System.out.println("3 - Сложно");
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int complexity = scanner.nextInt();
        //создаем новый массив размером 7 знаков
        int[] numbers = new int[7];
        String message = "Продолжите числовой ряд: ";


        //формируем уровень сложности - Легко
        if (complexity==1){
            System.out.println(message);
            int first_element = random.nextInt(85);
            //заполняем массив
            for (int i = 0; i < numbers.length; i++){
                first_element += 2;
                numbers[i] = first_element;
            }
            //Выводим массив без последней цифры
            for (int i = 0; i < 6; i++){
                System.out.print(numbers[i]);
                System.out.print(" ");
            }
            System.out.println("?");

            //Три попытки на угадывания числа
            for (int attempt = 1; attempt < 4; attempt++){
                int next_numbers = scanner.nextInt();
                if (next_numbers == numbers[6]){
                    System.out.println("Правильный ответ");
                    break;
                }
                else if (attempt ==3){
                    System.out.println("Вы исчерпали попытки. Правильный ответ был:");
                    for (int i = 0; i < 7; i++) {
                        System.out.print(numbers[i]);
                        System.out.print(" ");
                        }
                }
                else {
                    System.out.println("Попробуйте еще раз.");
                }
            }
        }



        //формируем уровень сложности - Средне
        else if (complexity==2){
            System.out.println(message);
           int first_element = random.nextInt(10);
            //заполняем массив
            for (int i = 0; i < numbers.length; i++){
                if ( i%2 ==0){
                    first_element += 1;
                }
                else {first_element *= 2;}
                numbers[i] = first_element;
            }
            //Выводим массив без последней цифры
            for (int i = 0; i < 6; i++){
                System.out.print(numbers[i]);
                System.out.print(" ");
            }
            System.out.println("?");

            //Три попытки на угадывания числа
            for (int attempt = 1; attempt < 4; attempt++){
                int next_numbers = scanner.nextInt();
                if (next_numbers == numbers[6]){
                    System.out.println("Правильный ответ");
                    break;
                }
                else if (attempt ==3){
                    System.out.println("Вы исчерпали попытки. Правильный ответ был:");
                    for (int i = 0; i < 7; i++) {
                        System.out.print(numbers[i]);
                        System.out.print(" ");
                    }
                }
                else {
                    System.out.println("Попробуйте еще раз.");
                }
            }
        }



        //формируем уровень сложности - Сложно
        else if (complexity==3){
            System.out.println(message);
            int first_element = random.nextInt(99);

            //заполняем массив
            for (int i = 0; i < numbers.length; i++){
                if ( first_element%2 ==0){
                    first_element /= 2;
                }
                else {first_element += 1;}
                numbers[i] = first_element;
            }

            //Выводим массив без последней цифры
            for (int i = 0; i < 6; i++){
                System.out.print(numbers[i]);
                System.out.print(" ");
            }
            System.out.println("?");

            //Три попытки на угадывания числа
            for (int attempt = 1; attempt < 4; attempt++){
                int next_numbers = scanner.nextInt();
                if (next_numbers == numbers[6]){
                    System.out.println("Правильный ответ");
                    break;
                }
                else if (attempt ==3){
                    System.out.println("Вы исчерпали попытки. Правильный ответ был:");
                    for (int i = 0; i < 7; i++) {
                        System.out.print(numbers[i]);
                        System.out.print(" ");
                    }
                }
                else {
                    System.out.println("Попробуйте еще раз.");
                }
            }
        }
        //Конец
        else { System.out.println("Конец"); }
    }
}
