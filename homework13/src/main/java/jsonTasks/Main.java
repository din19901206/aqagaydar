package jsonTasks;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){
        List<Person> personList = getPersonFromJson();
        int[] numbersArray = new int[1000000];
        PhonesMain phonesMain = new PhonesMain();
        phonesMain.fillArray(numbersArray);
        List<String> result = getRealPhones(numbersArray);
        System.out.println("Количество номеров " + result.size());
        result.forEach(x-> System.out.println(x));
    }

    public static List<String> getRealPhones(int[] numbers){
        List<String> result = new ArrayList<>();
        for (int i=0; i<=numbers.length - 11;i++){
            if(numbers[i]==8){
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j<11;j++){
                    sb.append(numbers[i+j]);
                }
                if (PhonesMain.isNumberGood(sb.toString())){
                    result.add(sb.toString());
                }
            }
        }
        return result;
    }

    public static List<Person> getPersonFromJson(){
        String jsonString = "{\n" +
                "  \"people\": [\n" +
                "    {\n" +
                "      \"firstName\": \"Вова\",\n" +
                "      \"lastName\": \"Петров\",\n" +
                "      \"age\": 30,\n" +
                "      \"loan\": 1800\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Семен\",\n" +
                "      \"lastName\": \"Семеныч\",\n" +
                "      \"age\": 30,\n" +
                "      \"loan\": 2000\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Саня\",\n" +
                "      \"lastName\": \"Петренко\",\n" +
                "      \"age\": 33,\n" +
                "      \"loan\": 2300\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Леха\",\n" +
                "      \"lastName\": \"Копытин\",\n" +
                "      \"age\": 18,\n" +
                "      \"loan\": 1700\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Алекс\",\n" +
                "      \"lastName\": \"Петров\",\n" +
                "      \"age\": 17,\n" +
                "      \"loan\": 1900\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Зоя\",\n" +
                "      \"lastName\": \"Белова\",\n" +
                "      \"age\": 20,\n" +
                "      \"loan\": 2000\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Мартын\",\n" +
                "      \"lastName\": \"Маслов\",\n" +
                "      \"age\": 22,\n" +
                "      \"loan\": 1750\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Лариса\",\n" +
                "      \"lastName\": \"Медведева\",\n" +
                "      \"age\": 23,\n" +
                "      \"loan\": 5000\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Никон\",\n" +
                "      \"lastName\": \"Зеленов\",\n" +
                "      \"age\": 24,\n" +
                "      \"loan\": 6000\n" +
                "    },\n" +
                "    {\n" +
                "      \"firstName\": \"Леон\",\n" +
                "      \"lastName\": \"Безруков\",\n" +
                "      \"age\": 50,\n" +
                "      \"loan\": 1600\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        List<Person> list = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray people = jsonObject.getJSONArray("people");
        for (int i=0; i<people.length(); i++){
            String name =  people.getJSONObject(i).getString("firstName");
            String lastName = people.getJSONObject(i).getString("lastName");
            int age = people.getJSONObject(i).getInt("age");
            double loan = people.getJSONObject(i).getDouble("loan");
            Person person = new Person(name, lastName, age, loan);
            list.add(person);
            System.out.println("В список был добавлен " + person);
            System.out.println("Статус выдачи кредита пользователя " + person.getFirstName() + " : " + checkLoan(person));
        }
        return list;
    }

    private static boolean checkLoan(Person person){
        return !person.getFirstName().equals("Bob") && person.getAge() >= 18 && person.getLoan() <= person.getAge() * 100;
    }
}