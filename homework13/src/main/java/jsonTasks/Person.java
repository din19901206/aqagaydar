package jsonTasks;

public class Person {
    private String firstName;
    private String lastName;
    private Integer age;
    private Double loan;

    public Person(String firstName, String lastName, int age, double loan) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.loan = loan;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public Double getLoan() {
        return loan;
    }

    @Override
    public String toString(){
        return lastName + " " + firstName + " " + age + " " + loan;
    }
}





















