package jsonTasks;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PhonesMain {
    private static List<String> codesOne = Arrays.asList(
            "123", "345", "444", "564", "643", "472", "675", "553", "889", "555"
    );

    private static List<String> codesTwo = Arrays.asList(
            "1423", "3456" , "4144", "9564", "4643", "2345", "2355", "7896", "1255", "0098"
    );

    private static final String PREFIX = "8";

    public static boolean isNumberGood(String number){
        return isNumberContainsValueFromList(number,codesOne)|| isNumberContainsValueFromList(number,codesTwo);

    }

    public static boolean isNumberContainsValueFromList(String number, List<String> codeList){
        boolean result = false;
        for (String code: codeList){
            String regex = PREFIX + code;
            regex += String.format("\\d{%d}",11-regex.length());
            if (number.matches(regex)){
                result = true;
                break;
            }
        }
        return result;
    }

    public void fillArray(int[] array){
        Random random = new Random();
        for (int i = 0; i< 1000000; i++){
            array[i] = random.nextInt(10);
        }
    }
}