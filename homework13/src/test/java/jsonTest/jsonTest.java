package jsonTest;

import jsonTasks.Main;
import jsonTasks.Person;
import jsonTasks.PhonesMain;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class jsonTest {

    @Test
    public void  TestGetPersonFromJson(){
        List <Person> personList = Main.getPersonFromJson();
        for (int i =0; i< personList.size();i++){
            Assertions.assertTrue(personList.get(i).getLoan()>0 && personList.get(i).getAge()>0);
        }
    }

    @Test
    public void TestGetRealPhones(){
        int[] numbersArray = new int[1000000];
        PhonesMain phonesMain = new PhonesMain();
        phonesMain.fillArray(numbersArray);
        List<String> resultTest = Main.getRealPhones(numbersArray);
        String goodNumber = resultTest.stream().filter(x->x.contains("0098")).findAny().orElse(null);
        Assertions.assertTrue(goodNumber.contains("0098"));
        Assertions.assertTrue(goodNumber.length()==11);
    }
}