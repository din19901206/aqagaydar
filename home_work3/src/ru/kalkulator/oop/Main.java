package ru.kalkulator.oop;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // одобрение кредита по параметрам: имя, возраст и сумма кредита
        System.out.println("Введите имя");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.println("Введите возраст");
        int age = scanner.nextInt();
        System.out.println("Введите сумму кредита");
        double credit = scanner.nextDouble();
        if (!name.equals("Bob") && age >=18 && credit <= 100*age){
            System.out.println("Кредит выдан");
        }else {
            System.out.println("Кредит не выдан");
        }
    }
}
