package ru.homework_6.oop;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calc {
    private String task;
    //список с числами
    private List<Double> numbers = new ArrayList<>();
    //список с приоритетами для операций
    private List<Integer> priority = new ArrayList<>();
    //список с символами самих операций +-,*
    private List<Character> actions = new ArrayList<>();


    //Формируем значение - task
    public void readString() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите выражение:" + "\n");
        String sourceTask = scanner.nextLine();
        //стираем пробелы
        task = sourceTask.replaceAll("\\s", "");

        //проверка по регулярному выражению
        String pattern = "^(([^-*/+]*\\d*\\.?\\d*[^-*/+])([-*/+)(]))*(\\d*\\.?\\d*)$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(task);

        //если по регулярному прошли успешно, то идем дальше
        if (m.matches()){
        }
        else { System.out.println("Введен некорректный символ");
            //завершаем программу
            System.exit(0);
        }
    }

    //получаем числа
    private void getNumbers() {
        String[] numbersArray = task.split("[-*/+)(]");
        List<String> numbersString = new ArrayList<>(Arrays.asList(numbersArray));
        numbersString.removeIf(x -> x.length() == 0);
        for (String item : numbersString) {
            numbers.add(Double.parseDouble(item));
        }
    }

    //определяем приоритет операции
    private void priorityOperations() {
        int priorityRate = 0;
        for (int i = 0; i < task.length(); i++) {
            if (task.charAt(i) == '(') {
                priorityRate = priorityRate + 2;
            }
            if (task.charAt(i) == ')') {
                priorityRate = priorityRate - 2;
            }
            if (task.charAt(i) == '-' || task.charAt(i) == '+') {
                actions.add(task.charAt(i));
                priority.add(priorityRate);

            }
            if (task.charAt(i) == '*' || task.charAt(i) == '/') {
                actions.add(task.charAt(i));
                priority.add(priorityRate + 1);

            }
        }
    }

    private Double doCalc(Character action, Double one, Double two) {
        Double result = 0.0;
        switch (action) {
            case '/':
                result = one / two;
                break;
            case '*':
                result = one * two;
                break;
            case '-':
                result = one - two;
                break;
            case '+':
                result = one + two;
                break;
        }
        return result;
    }

    private Double loop() {
        while (priority.size() > 0) {
            int max = Collections.max(priority);
            int index = priority.indexOf(max);
            Double result = doCalc(actions.get(index), numbers.get(index), numbers.get(index + 1));
            priority.remove(index);
            actions.remove(index);
            numbers.remove(index);
            numbers.set(index, result);

        }
        return null;
    }

    public Double doMagic() {
        getNumbers();
        priorityOperations();
        loop();
        return numbers.get(0);
    }

    public static void main(String[] args) {
        Calc Calculator = new Calc();
        Calculator.readString();
        Double result = Calculator.doMagic();
        System.out.print("Ответ:" + "\n" + result);
    }
}
