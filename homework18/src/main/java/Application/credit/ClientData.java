package Application.credit;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class ClientData {
    public String name;
    public Integer age;
    public Integer sum;
    public Long id;

    public void saveClient(String path, ObjectMapper mapper) throws IOException {
        mapper.writeValue(new File(path), this);
    }
}
