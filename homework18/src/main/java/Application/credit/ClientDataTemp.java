package Application.credit;

import java.util.Date;

public class ClientDataTemp {
    public String name;
    public Integer age;
    public Integer sum;

    public ClientData toClient(){
        ClientData temp = new ClientData();
        temp.age = age;
        temp.sum = sum;
        temp.name = name;
        temp.id = new Date().getTime();
        return temp;
    }
}
