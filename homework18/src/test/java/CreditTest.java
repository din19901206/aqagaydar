import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static io.restassured.RestAssured.given;

public class CreditTest {

    @DataProvider
    private Object[][] users(){
        return new Object[][]{
                {"Vova", 40, 500},
                {"Sahsa", 50, 20},
                {"Elena", 15, 300},
                {"Oleg", 20, 1800},
        };
    }

    //РАБОТАЕТ
    @Test(dataProvider = "users")
    public void createClient(String name, Integer age, Integer sum){
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("name", name);
        bodyJson.put("age", age);
        bodyJson.put("sum", sum);

        given().contentType(ContentType.JSON).body(bodyJson.toString()).post("http://localhost:8080/clients")
                .then().log().all();
    }

    //РАботает
    @Test
    public void deleteClient(){
        given().get("http://localhost:8080/clients/delete/1650424903656").then().log().all().assertThat().statusCode(200);
    }

    //Работает
    @Test
    public void check() throws IOException{
        System.out.println(getResult());
    }

    public String getResult() throws IOException {
        Path path = Paths.get("D:\\git\\aqagaydar\\homework18\\src\\main\\resources1650424903669");
        int age = 0;
        int sum = 0;
        String name = "";
        String[] params = Files.readAllLines(Paths.get(String.valueOf(path))).toString().split(",");
        for(int i = 0; i < params.length; i++){
            if(params[i].contains("age")){
                String[] values1 = params[i].split(":");
                age = Integer.parseInt(values1[1]);
            }
            if(params[i].contains("sum")){
                String[] values2 = params[i].split(":");
                sum = Integer.parseInt(values2[1]);
            }
            if(params[i].contains("name")){
                String[] values3 = params[i].split(":");
                name = values3[1].replace("\"", "");
            }
        }
        return  resultCredit(name,age,sum);
    }

    private String resultCredit(String name, int age, int summ) {
        return (age >= 18) && (!name.equals("Bob")) && (summ < age * 100) ? "Кредит одобрен"
                : "Отказано в кредите";
    }

}
