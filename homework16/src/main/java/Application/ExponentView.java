package Application;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExponentView {
    @GetMapping(path = {"/multi/", "/multi/{number}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String multi(@PathVariable(required = false) Integer number){
        JSONObject response = new JSONObject();
        if(number!=null){
            response = ExponentLogic.exponentNumber(number);
        } else {
            response.put("error", "Напишите цифру, например /multi/2");
        }
        return response.toString();
    }
}
