import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class SpringTest {

    @Test
    public void response(){
        Response response = given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/2")
                .then().log().body()
                .extract().response();
        JsonPath jsonPath = response.jsonPath();
        String one = jsonPath.getString("1");
        String two = jsonPath.getString("2");
        String three = jsonPath.getString("3");
        String four = jsonPath.getString("4");
        Assertions.assertEquals("2",one);
        Assertions.assertEquals("4",two);
        Assertions.assertEquals("8",three);
        Assertions.assertEquals("16",four);
    }

    @Test
    public void body(){
        given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/2")
                .then().log().body()
                .body("1",equalTo("2"))
                .body("2", equalTo("4"))
                .body("3", equalTo("8"))
                .body("4", equalTo("16"));
    }

    @Test
    public void  pojo(){
        Pojo pojo = given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/2")
                .then().log().body()
                .extract().body().jsonPath().getObject("", Pojo.class);
        Assertions.assertEquals("2",pojo.get_1());
        Assertions.assertEquals("4",pojo.get_2());
        Assertions.assertEquals("8",pojo.get_3());
        Assertions.assertEquals("16",pojo.get_4());
    }

    @Test
    public void empty(){
        given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/")
                .then().log().body()
                .body("error",equalTo("Напишите цифру, например /multi/2"));
    }

    @Test
    public void error404(){
        given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/test/")
                .then().log().body()
                .assertThat().statusCode(404);
    }
}



















