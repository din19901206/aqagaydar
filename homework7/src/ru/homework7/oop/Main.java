package ru.homework7.oop;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        //запрашиваем фразу для шифрования
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите фразу для шифрования на латинице:");
        String text = scanner.nextLine();

        //генерируем рандомный ключ
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);
        SecretKey key = kgen.generateKey();
        System.out.println("Зашифрованная фраза:");

        //создаем encoder1  - передаем в него фразу и ключ для шифрования
        Encoder encoder1 = new Encoder(text, key);
        System.out.println("\n" + "Расшифрованная фраза:");
        //создаем decoder1  - передаем в него уже зашифрованную фразу и ключ для расшифровки
        Decoder decoder1 = new Decoder(encoder1.bytes, key);
    }
}
