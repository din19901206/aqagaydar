package ru.homework7.oop;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Decoder {
    public Decoder(byte[] bytes, SecretKey key) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        //расшифровываем с помощью ключа key и алгоритма AES
        Cipher decriptCipher = Cipher.getInstance("AES");
        decriptCipher.init(Cipher.DECRYPT_MODE, key);
        byte[]  decriptedBytes = decriptCipher.doFinal(bytes);
        for (byte b: decriptedBytes){
            System.out.print((char)b);
        }
    }
}
