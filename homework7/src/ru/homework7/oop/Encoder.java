package ru.homework7.oop;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Encoder {
    public String text;
    public SecretKey key;
    public byte[] bytes;

    public Encoder(String text, SecretKey key) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        this.text = text;
        this.key = key;
        //шифруем с помощью ключа key и алгоритма AES
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE,key);
        byte[] bytes = cipher.doFinal(text.getBytes());
        this.bytes = bytes;
        for (byte b : bytes){
            System.out.print(b);
        }
    }
}
