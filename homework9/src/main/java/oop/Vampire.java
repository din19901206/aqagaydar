package oop;

public class Vampire extends Citizen{
    public Vampire(String name, String lastname) {
        race = Race.VAMPIRE;
        this.name = name;
        this.lastname = lastname;
    }
}
