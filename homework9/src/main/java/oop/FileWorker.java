package oop;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileWorker {
    private FileWriter file;

    public FileWorker(){
        try {
            file = new FileWriter("history.txt");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void writeToFile(String text) throws IOException {
        file.write(text + "\n");
        //записываем все,что есть в буфере. Если не указать, то последняя строка в файле обрезается.
        file.flush();
    }

    public void readerToFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("history.txt"));
        String line;
        while ((line= br.readLine()) != null){
            System.out.println(line);
        }
        //закрываем поток
        br.close();
    }
}





















