package oop;

public class Headhunter extends Citizen{
    public Headhunter(String name, String lastname) {
        race = Race.HEADHUNTER;
        this.name = name;
        this.lastname = lastname;
    }
}
