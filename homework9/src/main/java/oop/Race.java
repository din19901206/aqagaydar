package oop;

public enum Race {
    PEASANT,
    WITCH,
    VAMPIRE,
    WEREWOLF,
    HEADHUNTER
}
